# Go pre-commit hooks

These are a few hooks for [pre-commit][pre-commit] by Yelp to run basic Golang
tools against simple Go repos.

The hooks available are:

  - `gofmt`
  - `gofix`
  - `govet`
  - `gobuild`
  - `gotest`


They do exactly what they sound like they do.

An example `.pre-commit-config.yaml`:

```yaml
-   repo: https://bitbucket.org/SamWhited/go-pre-commit.git
    sha: HEAD
    hooks:
      -   id: gofmt
      -   id: gobuild
```

[pre-commit]: http://pre-commit.com/

## License

It's just a YAML file and a Bash one-liner… use them however the hell you want.
